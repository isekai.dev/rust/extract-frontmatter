# extract-frontmatter <!-- omit in toc -->

- [Overview](#overview)
    - [Versioning](#versioning)
    - [Repository information](#repository-information)
- [Usage](#usage)

## Overview

A Rust library that allows a user to extract an arbitrary number of lines of "front-matter" from the start of any
multiline string.

Note that absolutely no parsing of extracted front-matter is performed; this is designed to output its results for
another library to then parse.

[![latest build](https://img.shields.io/gitlab/pipeline-status/isekai/libraries/rust/extract-frontmatter?style=for-the-badge&logo=gitlab)](https://gitlab.com/isekai/libraries/rust/extract-frontmatter/-/pipelines/main/latest)
[![docs.io documentation](https://img.shields.io/docsrs/extract-frontmatter?style=for-the-badge&logo=docsdotrs)](https://docs.rs/extract-frontmatter)
[![coverage](https://img.shields.io/gitlab/coverage/rust/extract-frontmatter/main?gitlab_url=https%3A%2F%2Fgitlab.com%2Fisekai%2Flibraries&style=for-the-badge&logo=gitlab)](https://gitlab.com/isekai/libraries/rust/extract-frontmatter/-/pipelines/main/latest)
[![crates.io version](https://img.shields.io/crates/v/extract-frontmatter?style=for-the-badge&logo=rust)](https://crates.io/crates/extract-frontmatter)
[![crates.io downloads](https://img.shields.io/crates/d/extract-frontmatter?style=for-the-badge)](https://crates.io/crates/extract-frontmatter)
[![dependents](https://img.shields.io/librariesio/dependents/cargo/extract-frontmatter?style=for-the-badge)](https://crates.io/crates/extract-frontmatter/reverse_dependencies)
[![dependencies](https://img.shields.io/librariesio/release/cargo/extract-frontmatter?style=for-the-badge)](https://crates.io/crates/extract-frontmatter/dependencies)
[![license](https://img.shields.io/crates/l/extract-frontmatter?style=for-the-badge)](https://gitlab.com/isekai/libraries/rust/extract-frontmatter/-/blob/main/LICENSE)

### Versioning

This project follows [Semantic Versioning principals] starting with `v1.0.0`

[Semantic Versioning principals]: https://semver.org/

### Repository information

This repository is located on [GitLab.com].

[GitLab.com]: https://gitlab.com/isekai/libraries/rust/extract-frontmatter

## Usage

Example usage is available on [docs.rs].

[docs.rs]: https://docs.rs/extract-frontmatter/latest/extract_frontmatter/struct.Extractor.html
