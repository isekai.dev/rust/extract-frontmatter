#![warn(clippy::cargo, clippy::nursery, clippy::pedantic)]

use extract_frontmatter::config::{Modifier, Splitter};
use extract_frontmatter::Extractor;
use std::borrow::Cow;

#[test]
fn leading_line() {
    let (actual, _) = Extractor::new(Splitter::DelimiterLine("-->"))
        .with_modifier(Modifier::StripFirstLine)
        .extract(concat!("<!--\n", "Front-matter line 1\n", "Front-matter line 2\n", "-->"));

    let expected: Cow<str> = Cow::Owned(String::from(concat!("Front-matter line 1\n", "Front-matter line 2\n")));

    assert_eq!(actual, expected);
}

#[test]
fn trailing_line() {
    let (actual, _) = Extractor::new(Splitter::LineIndex(3)).with_modifier(Modifier::StripLastLine).extract(concat!(
        "Front-matter line 1\n",
        "Front-matter line 2\n",
        "---"
    ));

    let expected: Cow<str> = Cow::Owned(String::from(concat!("Front-matter line 1\n", "Front-matter line 2\n")));

    assert_eq!(actual, expected);
}

#[test]
fn leading_and_trailing_lines() {
    let (actual, _) = Extractor::new(Splitter::LineIndex(4))
        .with_modifier(Modifier::StripFirstLine)
        .with_modifier(Modifier::StripLastLine)
        .extract(concat!("<!--\n", "Front-matter line 1\n", "Front-matter line 2\n", "-->"));

    let expected: Cow<str> = Cow::Owned(String::from(concat!("Front-matter line 1\n", "Front-matter line 2\n")));

    assert_eq!(actual, expected);
}

#[test]
fn trailing_and_leading_lines() {
    let (actual, _) = Extractor::new(Splitter::LineIndex(4))
        .with_modifier(Modifier::StripLastLine)
        .with_modifier(Modifier::StripFirstLine)
        .extract(concat!("<!--\n", "Front-matter line 1\n", "Front-matter line 2\n", "-->"));

    let expected: Cow<str> = Cow::Owned(String::from(concat!("Front-matter line 1\n", "Front-matter line 2\n")));

    assert_eq!(actual, expected);
}
